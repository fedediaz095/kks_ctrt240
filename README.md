# KKS_CTRT240

Este es un interprete de codigo kks de la central 240Mw de Rio Turbio

----INSTRUCCIONES WINDOWS-----


A tener en cuenta:
-Ingresar todos los dígitos de kks
-El programa no soporta notación de instrumentacion

Modo de ejecución: 

La carpeta a la que se debe acceder es “kks_app_2.0_WINDOWS”, luego a “kks_2.0”.
Ejecutar programa “kks_2.0.exe”

-----INSTRUCCIONES MAC-----

Instrucciones:

Este es un interprete sencillo del código KKS de CTRT.

A tener en cuenta:
-Ingresar todos los dígitos de kks
-El programa no soporta notación de instrumentacion

Modo de ejecución: 

MAC
La carpeta a la que se debe acceder es “kks_app_2.0_MAC”, luego
Ejecutar programa “kks_2.0”.

IMPORTANTE! : Tener apretado la tecla CONTROL para abrir el archivo

Si sale error al abrir o dice que el desarrollador no es confiable (Mac): 
Ingresar a preferencias del sistema > privacidad y seguridad > abrir candado > “App Store y desarrolladores no identificados” >  abrir / permitir

Si dice que no hay aplicación para ejecutarlo:
Seleccionar aplicación > usar la aplicación “Terminal”  (debajo debe estar marcado “Activar: todas las aplicaciónes”).
