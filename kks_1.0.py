def main():
    # definicion de los diccionarios
    unit = {
        '10' : 'Comun a unidad 1 y 2',
        '11' : 'Unidad 1',
        '12' : 'Unidad 2',
        }
    system = {
        "AC_" :	"SUBESTACIÓN 220KV-245KV",
        "AQA" :	"Medición de energía",
        "AR_" :	"EQUIPO DE PROTECCIÓN DE RED ELÉCTRICA Y DISTRIBUCIÓN",
        "AS_" :	"PANEL    Y    ARMARIO    DESCENTRALIZADO    DE    RED    ELÉCTRICA    Y DISTRIBUCIÓN",
        "AXP" :	"Sistema de protección catódica",
        "BAA" :	"Evacuación de potencia del generador de TV",
        "BAC" :	"Interruptor automático del generador de TV",
        "BAT" :	"Transformador de generación",
        "BAW" :	"Red general de tierras",
        "BAY" :	"Protecciones y control eléctrico",
        "BBA" :	"Cuadros de distribución normales de MT",
        "BBB" :	"Barras de fase  agrupadas de MT",
        "BBT" :	"Transformadores auxiliares",
        "BBW" :	"Sistema de puesta a tierra de transformadores auxiliares",
        "BDA" :	"Distribución de emergencia de MT",
        "BFA" :	"Cuadros de distribución normales de BT",
        "BFB" :	"Barras de fase  agrupadas de BT",
        "BFT" :	"Transformadores de distribución de MT/BT",
        "BJA" :	"Centros de control de motores normales de BT",
        "BLA" :	"Cuadros de distribución normales de alumbrado",
        "BLL" :	"Panel normal de alumbrado",
        "BLS" :	"Alumbrado de seguridad",
        "BLT" :	"Transformadores normales de alumbrado",
        "BMA" :	"Cuadros de distribución de emergencia de BT",
        "BMB" :	"Centros de control de motores de emergencia de BT",
        "BMC" :	"Barras de fase  agrupadas de BT de emergencia",
        "BMT" :	"Transformadores de distribución de MT/BT de emergencia",
        "BNA" :	"Cuadros de distribución de alumbrado de emergencia",
        "BNT" :	"Transformadores de alumbrado de emergencia",
        "BRA" :	"Cuadro de distribución permanente de 115 V",
        "BRT" :	"Transformadores de bypass de 380/115 V",
        "BRU" :	"Inversores de 115 V",
        "BTA" :	"Batería de 110 V CC",
        "BTB" :	"Batería de 220 V",
        "BTL" :	"Sistema de 110 V CC- Cargador de batería",
        "BTM" :	"Sistema de 220 V CC - Cargador de batería",
        "BUA" :	"Distribución de 110 V CC",
        "BUB" :	"Distribución de 220 V CC",
        "CA_" :	"ENCLAVAMIENTOS DE PROTECCIÓN",
        "CBD" :	"Almacenamiento y transporte de sólidos",
        "CBP" :	"Sincronización y acoplamiento del generador de TV",
        "CC_" :	"ACONDICIONAMIENTO DE SEÑALES BINARIAS",
        "CD_" :	"INTERFAZ DE CONTROL DE ACCIONAMIENTO",
        "CE_" :	"ALERTAS Y ALARMAS",
        "CF_" :	"GRABACIÓN, MEDIDAS",
        "CG_" :	"CONTROL DE BUCLE CERRADO (EXCL. SECCIÓN DE POTENCIA)",
        "CH_" :	"PROTECCIÓN",
        "CHA" :	"Protecciones del generador y del transformador",
        "CHB" :	"Protecciones del generador y del transformador",
        "CHC" :	"Protecciones del generador y del transformador",
        "CHD" :	"Protecciones del generador y del transformador",
        "CHE" :	"Protección",
        "CHF" :	"Protección",
        "CHG" :	"Protección",
        "CHH" :	"Protección",
        "CHI" :	"Protección",
        "CHJ" :	"Protección",
        "CHK" :	"Protección",
        "CHL" :	"Protección",
        "CHM" :	"Protección",
        "CHN" :	"Protección",
        "CHO" :	"Protección",
        "CHP" :	"Protección",
        "CHQ" :	"Protección",
        "CHR" :	"Protección",
        "CHS" :	"Protección",
        "CHT" :	"Protección",
        "CHU" :	"Protección",
        "CHV" :	"Protección",
        "CHW" :	"Protección",
        "CHX" :	"Protección",
        "CHY" :	"Protección",
        "CJ_" :	"AUTOMATIZACIÓN  DEL  BLOQUE  GENERAL,  NIVEL  DE  COORDINACIÓN DE LA UNIDAD, ARMARIO DE CONTROL DE LA TURBINA",
        "CJA" :	"Sistemas automáticos principales (incluyendo cabinas)",
        "CJD" :	"Control del arranque y de los puntos de consigna (incluyendo cabinas)",
        "CJF" :	"Sistema de control de caldera (incluyendo cabinas)",
        "CJJ" :	"Cabina de I&C para el turbogrupo",
        "CJK" :	"Cabina de I&C para el turbogrupo",
        "CJL" :	"Cabina de I&C para el turbogrupo",
        "CJM" :	"Cabina de I&C para el turbogrupo",
        "CJN" :	"Cabina de I&C para el turbogrupo",
        "CJU" :	"Cabina de I&C para otros equipos mecánicos pesados",
        "CJV" :	"Cabina de I&C para otros equipos mecánicos pesados",
        "CJW" :	"Cabina de I&C para otros equipos mecánicos pesados",
        "CJX" :	"Cabina de I&C para otros equipos mecánicos pesados",
        "CJY" :	"Cabina de I&C para otros equipos mecánicos pesados",
        "CS_" :	"ARMARIOS DE SCD. ARMARIOS DE E/S",
        "CVA" :	"Cajas y cubículos de interconexión",
        "CWA" :	"Sala de control: consolas de control",
        "CWF" :	"Sala de control: paneles de control",
        "CWQ" :	"Sala de control impresoras",
        "CX_" :	"ESTACIONES DE CONTROL LOCAL",
        "CYA" :	"Sistema telefónico",
        "CYB" :	"Sistema de megafonía",
        "CYD" :	"Sistemas de comunicación de datos",
        "CYE" :	"Sistema de protección contra incendios",
        "CYF" :	"Distribución temporal, sistema de relojes",
        "CYN" :	"Interfono",
        "EAC" :	"Sistema de transporte",
        "EAD" :	"Sistema de apilado",
        "EAE" :	"Área de almacenamiento",
        "EAF" :	"Sistema de recogida de carbón",
        "EAT" :	"Sistema de pesado de carbón",
        "EAU" :	"Sistema de muestreo de carbón",
        "EBA" :	"Sistema de transporte",
        "EBB" :	"Sistema de mezclado",
        "EBC" :	"Sistema de trituración",
        "EBD" :	"Sistema de filtrado",
        "EBE" :	"Sistema de descarga y separación",
        "EBR" :	"Sistema de eliminación de resíduos",
        "EBT" :	"Sistema de pesado de carbón",
        "EBU" :	"Sistema de muestreo de carbón",
        "ECT" :	"Sistema de pesado",
        "ECU" :	"Sistema de muestreo",
        "ED_" :	"Sistema de caliza",
        "EGA" :	"Descarga de gasoil",
        "EGB" :	"Almacenamiento de gasoil",
        "EGC" :	"Sistema de bombeo",
        "EGD" :	"Tuberías de gasoil",
        "EGS" :	"Tratamiento de gasoil",
        "EGT" :	"Sistema de calentamiento gas-oil",
        "EKA" :	"Tubería de acometida",
        "EKC" :	"Sistema de calentamiento",
        "EKD" :	"Estación de regulación y medida, ERM",
        "EKE" :	"Lavado de gas",
        "EKG" :	"Tuberías de gas",
        "EKH" :	"Compresor de gas",
        "EKT" :	"Calentamiento de gas (CC) (calentador de gas) (medio de calefacción)",
        "EKW" :	"Sistema de suministro de fluido de sellado",
        "ERC" :	"Sistema de suministro de gas",
        "ETD" :	"Sistema de transporte de la ceniza de fondo",
        "ETE" :	"Sistema de almacenamiento de la ceniza de fondo",
        "ETG" :	"Sistema de transporte de la ceniza volante",
        "ETH" :	"Sistema de almacenamiento de la ceniza volante",
        "ETP" :	"Sistemas de generación y distribución del aire de fluidización",
        "ETV" :	"Sistema de suministro de lubricantes",
        "ETX" :	"Sistema de suministro del fluido de control y de protección",
        "ETY" :	"Equipos de protección y control",
        "GAC" :	"Tuberías",
        "GAD" :	"Tanque de Almacenamiento",
        "GAF" :	"Bombeo de agua de aportación",
        "GB_" :	"SISTEMA DE TRATAMIENTO",
        "GC_" :	"Producción     de     agua     desmineralizada     (planta     paquete     de     agua desmineralizada)",
        "GCF" :	"Ósmosis inversa + EDI y equipo auxiliar",
        "GCN" :	"Dosificación química a la planta de tratamiento de agua",
        "GD_" :	"OTROS SISTEMAS DE TRATAMIENTO",
        "GDD" :	"Pretratamiento y equipo auxiliar",
        "GDG" :	"Sistema de evaporación",
        "GDQ" :	"Planta de producción de agua potable",
        "GHA" :	"Agua de servicio",
        "GHC" :	"Almacenamiento y distribución de agua desmineralizada",
        "GKB" :	"Agua potable",
        "GMA" :	"Aguas residuales (aceitosas)",
        "GMB" :	"Drenajes y purgas de aguas de proceso",
        "GN_" :	"Tratamiento  de  efluentes  industriales  (planta  paquete),  planta  de  tratamiento de drenajes de proceso",
        "GR_" :	"Planta de tratamiento de agua sanitarias",
        "GU_" :	"Recogida y drenaje de agua de lluvia",
        "HAC" :	"Economizadores",
        "HAD" :	"Evaporadores",
        "HAG" :	"Sistema de circulación de la caldera",
        "HAH" :	"Sobrecalentadores",
        "HAN" :	"Drenajes y venteos de caldera",
        "HB_" :	"Cerramiento, soportes e interior de la caldera",
        "HCB" :	"Sopladores de hollín",
        "HDA" :	"Recogida de cenizas de fondo",
        "HDB" :	"Sistema de retorno de cenizas de fondo",
        "HDD" :	"Sistema mecánico de tratamiento de polvo (filtro de mangas)",
        "HDF" :	"Ciclón de eliminación de ceniza volante y sistema de retorno (ceniza volante)",
        "HFA" :	"Silo de carbón",
        "HFB" :	"Sistema de alimentación",
        "HHH" :	"Sistema de material del lecho (sistema de arena)",
        "HHL" :	"Suministro de aire de combustión",
        "HHM" :	"Sistema de generación de aire a alta presión",
        "HHN" :	"Sistema de distribución de aire de alta presión",
        "HHS" :	"Aire de fluidización para la alimentación de combustible",
        "HHW" :	"Suministro de aire de sellado",
        "HJA" :	"Quemadores de arranque",
        "HJG" :	"Distribución y regulación de presión de gas",
        "HJL" :	"Aire secundario a quemadores",
        "HJQ" :	"Aire de sellado y refrigeración de los quemadores",
        "HLA" :	"Conductos aire primario",
        "HLB" :	"Ventiladores de aire de combustión",
        "HLC" :	"Calentamiento de aire con vapor",
        "HLD" :	"Calentamiento de aire con gases de chimenea",
        "HLV" :	"Sistema de suministro de aceite lubricante a los ventiladores",
        "HNA" :	"Conductos",
        "HNC" :	"Sistema de ventiladores de tiro inducido",
        "HNE" :	"Chimenea",
        "HNY" :	"Equipo de protección y control del escape de gases de combustión",
        "HRA" :	"Silos de caliza",
        "HRB" :	"Sistema de alimentación de caliza",
        "HRC" :	"Sistema de aire de fluidización",
        "HSJ" :	"Sistema de amoníaco",
        "HSK" :	"Sistema de distribución de amoníaco",
        "HST" :	"Sistema de lavado",
        "HY_" :	"Control y protección de la caldera",
        "HYC" :	"Seguridad de la caldera",
        "HYR" :	"Instrumentación y control analógicos de la caldera",
        "LAA" :	"Almacenamiento y desaireación",
        "LAB" :	"Tuberías de agua de alimentación",
        "LAC" :	"Bombas de agua de alimentación",
        "LAE" :	"Atemperación de AP",
        "LAV" :	"Lubricación de la bomba de agua de alimentación",
        "LAW" :	"Sellado de la bomba de agua de alimentación",
        "LBA" :	"Vapor principal",
        "LBD" :	"Vapor de extracción / baja presión",
        "LBF" :	"Estación de reducción de alta presión",
        "LBG" :	"Distribución de vapor auxiliar",
        "LBQ" :	"Extracción de vapor de AP para los calentadores de agua de alimentación",
        "LBS" :	"Extracción de vapor de BP para los calentadores de agua de alimentación",
        "LCA" :	"Tuberías de condensado",
        "LCB" :	"Bombas de condensado",
        "LCC" :	"Sistema de calentamiento de condensado",
        "LCE" :	"Sistema de atemperación con condensado",
        "LCH" :	"Drenajes de calentadores de AP",
        "LCJ" :	"Drenajes de calentadores de BP",
        "LCL" :	"Sistema de drenajes del generador de vapor",
        "LCM" :	"Drenajes limpios",
        "LCN" :	"Condensado del vapor auxiliar",
        "LCP" :	"Aporte de condensado (almacenamiento y tuberías)",
        "LCQ" :	"Purga del generador de vapor",
        "LCR" :	"Distribución de agua de condensado",
        "LD_" :	"PLANTA DE TRATAMIENTO DE CONDENSADO",
        "MAB" :	"Turbina de vapor",
        "MAD" :	"Cojinetes de la turbina de vapor",
        "MAG" :	"Condensador principal",
        "MAJ" :	"Vacío del condensador",
        "MAK" :	"Virador de la TV",
        "MAL" :	"Drenajes y venteos de turbina",
        "MAN" :	"Estación de bypass de turbina",
        "MAP" :	"Tuberías  de  descarga  de  vapor  de  bypass  de  la  turbina  y  DUMP  TUBES asociados",
        "MAV" :	"Lubricación de la TV, aceite de elevación",
        "MAW" :	"Vapor de sello de cierres de la TV",
        "MAX" :	"Fluido de control de la TV (sistema electrohidráulico)",
        "MAY" :	"Protecciones de la turbina de vapor",
        "MKA" :	"Generador completo",
        "MKC" :	"Excitación y regulador automático de tensión del generador",
        "MKD" :	"Cojinetes del generador",
        "MKF" :	"Refrigeración del líquido del estator y del rotor del generador",
        "MKV" :	"Sistema de aceite de lubricación del generador",
        "MKW" :	"Suministro de fluido de sellado del generador",
        "MKX" :	"Sistema de suministro de fluido del generador para control",
        "MKY" :	"Sistema de protección del generador (protección mecánica con generador de disparo)",
        "MV_" :	"Sistema de tratamiento de aceite de lubricación",
        "NAA" :	"Sistema de tuberías (vapor)",
        "NAB" :	"Sistema de tuberías (condensado)",
        "NAD" :	"Sistema de transmisión de calor de proceso",
        "NDA" :	"Sistema de tuberías (aporte)",
        "NDB" :	"Sistema de tuberías (retorno)",
        "NDC" :	"Sistema de bombas de agua caliente de proceso",
        "NDD" :	"Transferencia de calor de proceso",
        "NDE" :	"Sistema de almacenamiento de agua caliente",
        "NDF" :	"Sistema de distribución",
        "PGA" :	"Tuberías (impulsión)",
        "PGB" :	"Tuberías (retorno)",
        "PGC" :	"Bombas",
        "PGD" :	"Enfriadores, cambiadores de calor",
        "PGJ" :	"Tanque de expansión",
        "QCA" :	"Subsistema de secuestrante de oxígeno",
        "QCB" :	"Subsistema de aminas",
        "QCC" :	"Subsistema de fosfato",
        "QCD" :	"Subsistema inhibidor de la corrosión",
        "QFA" :	"Producción de aire de instrumentación y control (si es distinto del SCA)",
        "QFB" :	"Distribución de aire de instrumentación y control",
        "QHA" :	"Producción de vapor auxiliar (caldera auxiliar)",
        "QHE" :	"Sistema de purga",
        "QHH" :	"Quemadores de la caldera auxiliar",
        "QJA" :	"Sistema de almacenamiento y distribución de nitrógeno",
        "QLB" :	"Distribución de vapor auxiliar (de la caldera auxiliar)",
        "QU_" :	"Control y muestreo químico",
        "RSA" :	"Limpieza   de   la   planta   de   calentamiento   de   agua   de   alimentación   y condensación (temporal)",
        "RSB" :	"Tuberías de soplado con vapor (temporal)",
        "RSC" :	"Limpieza de caldera (temporal)",
        "SAA" :	"Ventilación de edificios",
        "SAC" :	"Ventilación del generador y del compartimiento de la turbina de vapor",
        "SAD" :	"Refrigeración forzada de la TV",
        "SAE" :	"Ventilación del generador y del compartimiento de la turbina",
        "SAK" :	"Sistemas de aire acondicionado, calefacción y ventilación de edificios",
        "SAM" :	"Ventilación del edificio de la turbina",
        "SAW" :	"Calentamiento de edificios",
        "SBB" :	"Traceado eléctrico",
        "SCA" :	"Producción de aire comprimido",
        "SCB" :	"Distribución de aire de servicios",
        "SGA" :	"Sistemas de agua para protección contra incendios",
        "SGC" :	"Sistemas de alivio de rociado",
        "SGE" :	"Sistemas de rociado",
        "SGG" :	"Sistema de refrigeración de techo y virola de tanque",
        "SGH" :	"Vapor de apagado a la alimentación de combustible",
        "SGJ" :	"Sistemas de protección contra incendios de CO2",
        "SGK" :	"Sistemas de protección contra incendios de agentes limpios gaseosos",
        "SGL" :	"Sistema de protección contra incendios de polvo",
        "SGY" :	"Sistema de control y detección",
        "SM_" :	"Grúas y elevadores",
        "SN_" :	"ASCENSORES",
        "SQ_" :	"INSTALACIÓN DE CARRETERAS",
        "ST_" :	"EQUIPOS DE TALLER Y LABORATORIO",
        "UA_" :	"ESTRUCTURAS DE LA SUBESTACIÓN",
        "UBE" :	"Estructuras para el edificio eléctrico",
        "UBF" :	"Estructuras para transformadores",
        "UC_" :	"ESTRUCTURAS DE CONTROL E INSTRUMENTACIÓN",
        "UEJ" :	"Instalaciones de combustible líquido",
        "UEN" :	"Instalaciones de combustible gas (salvo Estación de Regulación y Medida)",
        "UER" :	"Instalaciones de combustible gas (ERM)",
        "UGD" :	"Estructuras para agua desmineralizada",
        "UGF" :	"Estructuras de agua para protección contra incendios",
        "UGG" :	"Estructuras para agua potable",
        "UGU" :	"Estructuras para efluentes",
        "UHA" :	"Estructuras de la caldera",
        "UHB" :	"Estructuras del recinto de la caldera",
        "ULA" :	"Estructuras para la casa de bombas de agua de alimentación",
        "UMA" :	"Edificio de turbina de vapor o común",
        "UMJ" :	"Edificio del generador diesel",
        "US_" :	"ESTRUCTURAS PARA SERVICIOS AUXILIARES",
        "UTH" :	"Estructuras para caldera auxiliar",
        "UYA" :	"Estructura para edificio de oficinas",
        "UYF" :	"Estructura para edificio de control de acceso",
        "UZ_" :	"ESTRUCTURAS  GENERALES  PARA  TRANSPORTE,  TRÁFICO,  VALLADO, JARDINES Y OTROS FINES",
        "XKA" :	"Grupo Electrógeno de BT",
        "XKB" :	"Grupo Electrógeno de MT",
        "XKC" :	"Excitatriz del generador",
        "XKD" :	"Rodamientos",
        "XKF" :	"Sistema de enfriamiento (líquido) del estator y del rotor",
        "XKQ" :	"Sistema de gases de escape",
        "XKV" :	"Sistema de suministro de fluido de lubricación",
        "XKW" :	"Sistema de suministro de fluido de sellado",
        "XKY" :	"Equipos de control y protección",
        "XRA" :	"Instalación de turbina de gas",
        }
    equipment = {
        "AA" : "Válvulas,  compuertas,  etc.,  incl.  actuadores,  también manuales;  discos  de  rotura,  purgadores  o  venteos automáticos. (válvula manual_válvula de retención_válvula de seguridad(_válvula solenoide_válvula motorizada_válvula operada)",
        "AB" : "Elemento aislante, esclusa, compuertas, barreras, persianas",
        "AC" : "Intercambiador de calor (incluidos atemperadores)",
        "AE" : "Grúas y engranajes para accionamiento, levantamiento, giro y variadores de velocidad",
        "AF" : "Transportadores continuos, alimentadores",
        "AG" : "Unidades de generación",
        "AH" : "Unidad   de   calentamiento acondicionado y enfriamiento de aire",
        "AJ" : "Equipos de reducción de tamaño",
        "AK" : "Equipos de compactado y empaquetado",
        "AM" : "Mezcladores, agitadores",
        "AN" : "Compresores, ventiladores, extractores",
        "AP" : "Bombas",
        "AS" : "Equipos de actuación no eléctricos",
        "AT" : "Equipos de limpieza rejillas, secado,    filtrado    y separación",
        "AU" : "Unidades de  frenado,  convertidores  (no eléctricos), acoplamientos, engranajes",
        "AV" : "Equipo de combustión (parrillas, quemadores)",
        "AW" : "Herramientas estáticas, equipos de tratamiento",
        "AX" : "Equipo de pruebas y supervisión para mantenimiento de la central",
        "BB" : "Equipo de  almacenamiento (tanques, depósitos, acumuladores, calderines, balsas, …)",
        "BE" : "Ejes (úniamente para construcción y mantenimiento)",
        "BF" : "Cimentaciones",
        "BN" : "Bombas de chorro, eyectores, inyectores",
        "BP" : "Restrictores  de  flujo,  limitadores,  orificios  (no  orificios de medida)",
        "BQ" : "Soportes, bastidores, penetraciones de tuberías",
        "BR" : "Tuberías, conductos, rampas, juntas de expansión",
        "BS" : "Silenciadores",
        "BT" : "Módulos de conversión catalítica de los gases de escape",
        "BU" : "Aislamiento",
        "CD" : "Densidad",
        "CE" : "Circuitos de medición directa de variables eléctricas intensidad_tensión_energía_frecuencia)",
        "CF" : "Flujo, caudal(_transmisor de flujo_caudalímetro_interruptor de flujo_orificio de flujo_flujo venturi_válvula de flujo_controlador de flujo_cristal de flujo)",
        "CG" : "Distancia, longitud, posición, dirección de rotación (_transmisor de posición)",
        "CH" : "Entrada manual como sensor manual (Alarma       contra       incendios manual)",
        "CK" : "Tiempo",
        "CL" : "Nivel (_válvula de control de nivel_transmisor de nivel_medidor de nivel_interruptor de nivel_controlador de nivel)",
        "CM" : "Humedad (_transmisor de punto de rocío)",
        "CP" : "Presión, vacio (_transmisor de presión_manómetro_interruptor de presión_controlador de presión_válvula de presión)",
        "CQ" : "Variables de calidad (análisis, propiedades materiales) que no sean CD, CM, CU (Concentración de CO2 en gas de  escape,  concentración  de contaminante)",
        "CS" : "Velocidad, frecuencia (mecánica), aceleración (Medidas de línea de eje)",
        "CT" : "Temperatura (_transmisor de temperatura_termopar_termómetro de resistencia_interruptor de temperatura_termómetro de cristal_termopozo_controlador de temperatura_válvula de temperatura)",
        "CU" : "Variables combinadas y otras (_valores corregidos)",
        "CW" : "Peso, masa",
        "CY" : "Vibración, expansión",
        "EA" : "Lazos abiertos de control (Indicación análoga)",
        "EG" : "Alarma y anunciadores (Alarma de peligro)",
        "EH" : "Alarma y anunciadores (Prealarma)",
        "EM" : "Ordenador de proceso (Valor     de     texto,     variables internas, etc.)",
        "EU" : "Acondicionamiento  de  señales  análogas  y  binarias combinadas (Estado)",
        "EW" : "Protección (Alarma  que  induce  disparo  o estado de reserva)",
        "FD" : "Densidad",
        "FE" : "Variables eléctricas",
        "FF" : "Caudal",
        "FG" : "Distancia, longitud, posición",
        "FK" : "Tiempo",
        "FL" : "Nivel",
        "FM" : "Humedad",
        "FP" : "Presión",
        "FQ" : "Variables de calidad",
        "FS" : "Velocidad, frecuencia",
        "FT" : "Temperatura",
        "FU" : "Otras variables",
        "FW" : "Peso",
        "FY" : "Vibración, expansión",
        "GA" : "Cajas de conexiones y penetraciones de cables/barras (Alta tensión)",
        "GB" : "Cajas de conexiones y penetraciones de cables/barras (Media tensión)",
        "GC" : "Cajas de conexiones y penetraciones de cables/barras (Baja tensión)",
        "GD" : "Cajas de conexiones y penetraciones de cables/barras (Control)",
        "GE" : "Cajas de conexiones y penetraciones de cables/barras (Medición)",
        "GF" : "Cajas de conexiones y penetraciones de cables/barras (Teléfono,     electrónica,     fibra óptica)",
        "GG" : "Cajas de conexiones y penetraciones de cables/barras (Otros)",
        "GH" : "Unidades de instalación eléctrica y de instrumentación y  control  identificadas  por  sistema  de  proceso  (p.ej. cubículos, cajas) (Cajas de conexiones cubículos,   armarios,   paneles del emplazamiento)",
        "GK" : "Pantallas   de   información   y   equipo   de   control   del operador  para  ordenadores  de  proceso  y  sistemas automatizados (Impresora,  pantalla  de  vídeo, teclados,  unidad,  estación  de operador o unidad de interfaz)",
        "GS" : "Equipo eléctrico: equipo del cuadro de distribución (si no se identifica dentro del equipo de proceso)",
        "GT" : "Transformador (_transformador de tensión_transformador de intensidad)",
        "GU" : "Convertidor",
        "GV" : "Equipo de pararrayos y puesta a tierra relacionado con la estructura",
        "GW" : "Equipo de alimentación eléctrica",
        "GZ" : "Soportes y canalizaciones para equipos eléctricos y de instrumentación y control",
        "HA" : "Conjuntos de maquinaria (estacionaria)",
        "HB" : "Conjuntos de maquinaria (rotativa)",
        "HD" : "Conjuntos de rodamientos",
        }
    # presentacion
    print("____________________________________")
    print("INTERPRETACION DE CODIGO KKS - CTRT")
    print("Version 1.0 - 2021")
    print("<Diaz F>")
    # ingreso de codigo, transformacion a string y a mayusculas
    i=True
    while (i==True):
        print("____________________________________")
        kks = input("Ingrese un codigo KKS: ")
        print("____________________________________")
        print("                                    ")
        kks = str(kks)
        kks = kks.upper()

        # separacion del codigo ingresado
        kks_unit = kks[0:2]
        kks_system = kks[2:5]
        kks_subsystem = kks[5:7]
        kks_equipment = kks[7:9]
        kks_equipment_number = kks[9:12]
        kks_unit = str(kks_unit)
        kks_equipment_number_int = int(kks_equipment_number)

        # buscar parte de los codigos en diccionarios

        if kks_unit in unit.keys():
            unidad = (unit[kks_unit])
            print(kks_unit + '   >>> ' + unidad)
        else:
            print(kks_unit + '   >>> ' + '(No disponible)')
            
        if kks_system in system.keys():
            sistema = (system[kks_system])
            print(kks_system + '  >>> ' + sistema)
        else:
            print(kks_system + '   >>> ' + '(No disponible)')

        print(kks_subsystem + '   >>> ' + 'Subsistema')

        if kks_equipment in equipment.keys():
            p = True
            equipo =  (equipment[kks_equipment])
            print(kks_equipment + '   >>> ' + equipo)
        else:
            p = False
            print(kks_equipment + '   >>> ' + '(No disponible)')

        if (p == True):
            if (kks_equipment == "AA"):
                if(kks_equipment_number_int < 200):
                    print(kks_equipment_number +  '  >>> ' + "Valvula manual o de retencion")
                elif(kks_equipment_number_int < 300):
                    print(kks_equipment_number +  '  >>> ' + "Valvula de control (neumatica)")
                elif(kks_equipment_number_int < 400):
                    print(kks_equipment_number +  '  >>> ' + "Valvula motorizadas (abiertas/cerradas)")
                elif(kks_equipment_number_int < 500):
                    print(kks_equipment_number +  '  >>> ' + "Valvula electrica de proceso")
                elif(kks_equipment_number_int < 600):
                    print(kks_equipment_number +  '  >>> ' + "Valvula de drenaje manual y venteo")
                elif(kks_equipment_number_int < 700):
                    print(kks_equipment_number +  '  >>> ' + "Valvula auto-regulada")
                elif(kks_equipment_number_int < 800):
                    print(kks_equipment_number +  '  >>> ' + "Valvula de raiz de instrumentacion")
                elif(kks_equipment_number_int < 900):
                    print(kks_equipment_number +  '  >>> ' + "Numero de equipo")
                elif(kks_equipment_number_int < 1000):
                    print(kks_equipment_number +  '  >>> ' + "Valvula de alivio y seguridad")
            else:
                print(kks_equipment_number +  '  >>> ' + 'Numero de equipo')

        print("____________________________________")

if __name__ == '__main__':
    main()